---
pagetitle: "Initial Setup"
#title: "\nCurso de R aplicado a datos espaciales\n"
#subtitle: "\nConfiguración Inicial\n"
#author: "\nEduard F. Martínez-González\n"
#date: "Centro de Investigaciones Económicas y Financieras | [CIEF](https://www.eafit.edu.co/cief) \n**Universidad EAFIT**"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---
```{r setup, include=FALSE}
# load packages
library(pacman)
p_load(here,knitr,tidyverse)

# option html
options(htmltools.dir.version = FALSE)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

## Antes de iniciar el curso...

### **1.** Instalar R  

### **2.** Instalar Rstudio

### **3.** Chequear instalación

<!------------------>
<!--- Instalar R --->
<!------------------>

# [1.] Instalar R

<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

R es uno de los lenguajes de programación más utilizados en investigación científica y con una creciente popularidad en los campos del aprendizaje automático y la minería de datos (Ver:[The Popularity of Data Science Software](http://r4stats.com/articles/popularity/)).

<!--------------------->
## Descargar el software

Primero debe descargar la versión de [R](https://cran.r-project.org/) compatible con el sistema operativo de su equipo:

+ Version 4.1.2 para [Mac](https://cran.r-project.org/bin/macosx/)
  
+ Version 4.1.2 para [Windows](https://cran.r-project.org/bin/windows/base/)

+ Version 3.6.3 para [Linux](https://cran.r-project.org/bin/linux/)

**Nota:** los usuarios Mac y Windows deben descargar la versión **4.1.2** del software y los usuarios Linux deben descargar la última versión de R que este disponible para la distribución de Linux sobre la que esten trabajando.

<!--------------------->
## Instalar R en Mac

![](pics/install_r_mac.gif)

<!--------------------->
## Instalar R en Windows

![](pics/install_r_windows.gif)

<!------------------------>
<!--- Instalar RStudio --->
<!------------------------>

# [2.] Instalar RStudio

<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

[RStudio](https://rstudio.com) es un entorno de desarrollo integrado (IDE) para el lenguaje de programación [R](https://www.r-project.org). En un lenguaje más practico, Rstudio brinda una interfaz de R más amigable con el usuario, lo que puede facilitar el aprendizaje.

<!--------------------->
## Descargar el software

[Aquí](https://www.rstudio.com/products/rstudio/download/preview/) puede obtener el instalador de **RStudio** compatible con su sistema operativo.

**Nota:** para los usuarios de Mac que tengan una versión anterior a **10.12.6** deben instalar una versión anterior de Rstudio. Puede obtener las versiones anteriores del software aquí: [Older versions Rstudio](https://www.rstudio.com/products/rstudio/older-versions/)

![](pics/download_rstudio.png)


<!--------------------->
## Instalar RStudio en Mac

![](pics/install_rstudio_mac.gif)

<!--------------------->
## Instalar RStudio en Windows

![](pics/install_rstudio_windows.gif)

<!---------------------------->
<!--- Chequear instalación --->
<!---------------------------->

# [3.] Chequear instalación
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

Para abrir los programas insralados, puede ir al buscador de programas en su  equipo y buscar **R** o **Rstudio** directamente.

<!--------------------->
## Chequear la versión de R
Después de abrir **Rstudio** debe escribir `R.version.string` sobre la pestaña **Console** y orpimir la tecla **Enter**
![](pics/check_windows.gif)

<!--------------->
<!--- Resumen --->
<!--------------->
# Hemos terminado...
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

☑ Instalamos R

☑ Instalamos RStudio

☑ Chequear instalación del software

<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.4em ; color:#CC0000 ; font-weight:bolde ; text-align:left}
.reveal section h3 {font-size:1.1em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section h4 {font-size:1.0em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.8em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:1.0em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
</style>

